"use client";
import React, { Component } from 'react'
import CardFuncionalidad from '../components/card-funcionalidad/card-funcionalidad';

export default class FuncionalidadesPage extends Component {

    render() {
        const funcionalidades = [
            {
                title: "Creá tu carta digital",
                text: "Actualización fácil y rápida del menú, evitando los costos de impresión y desperdicio de papel."
            },
            {
                title: "Creá tu carta digital",
                text: "Actualización fácil y rápida del menú, evitando los costos de impresión y desperdicio de papel."
            },
            {
                title: "Creá tu carta digital",
                text: "Actualización fácil y rápida del menú, evitando los costos de impresión y desperdicio de papel."
            },
        ]

        return (
            <>
                <h2 className='qresto-roboto-slab fw-bold'>Potenciá tu negocio con estas funciones</h2>
                {
                    funcionalidades.map(((funcionalidad, index) => (
                            <CardFuncionalidad key={index} funcionalidad={funcionalidad} imageRight={index%2==0} />
                        )
                    ))
                }
            </>
        )
    }
}
