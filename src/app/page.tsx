"use client";
import Image from 'next/image';
import Header from './components/header/header';
import styles from './page.module.css';
import 'bootstrap/dist/css/bootstrap.css';
import NavBar from './components/navbar/navbar';
import { BrowserRouter as Router, Route, Routes, RouteProps } from 'react-router-dom';
import FuncionalidadesPage from './pages/funcionalidades';

export default function Home() {
  const links = [
    { name: 'Inicio', route: '/' },
    { name: 'Beneficios', route: '/beneficios' },
    { name: 'Contacto', route: '/contact' },
    { name: 'algo', route: 'aasd'}
  ];

  return (
    <Router>
      <main className={styles.main}>
        <div className={styles.description + ' d-flex flex-column w-100'}>
          <Header />
          <NavBar links={links} />

          {/* Define las rutas */}
          <Routes>
            <Route path="/" Component={HomePage} />
            <Route path="/beneficios" Component={FuncionalidadesPage} />
            <Route path="/contact" Component={ContactPage} />
          </Routes>
          
        </div>
      </main>
    </Router>
  );
}

// Componentes de las páginas correspondientes a las rutas
const HomePage = () => <h1>Inicio</h1>;
const AboutPage = () => <h1>Acerca de</h1>;
const ContactPage = () => <h1>Contacto</h1>;
