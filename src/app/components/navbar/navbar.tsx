"use client"
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './navbar.css'

interface NavBarProps {
  links: { name: string; route: string }[];
}

interface NavBarState {
  activeLink: string;
}

export default class NavBar extends Component<NavBarProps, NavBarState> {
  constructor(props: NavBarProps) {
    super(props);
    this.state = {
      activeLink: '/',
    };
  }

  handleClick = (route: string) => {
    this.setState({ activeLink: route });
  };

  render() {
    const { links } = this.props;
    const { activeLink } = this.state;

    return (
      <nav className='w-50 mb-3'>
        <ul className='d-flex flex-row justify-content-between w-100 p-0'>
          {links.map((link, index) => (
            <li key={index}>
              <Link
                to={link.route}
                className={'nav-link qresto-roboto-slab fw-bold ' + (link.route === activeLink ? 'nav-link-active' : '')}
                onClick={() => this.handleClick(link.route)}
              >
                <span> {link.name} </span>
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    );
  }
}

