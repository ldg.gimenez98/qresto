"use client";
import React, { Component } from 'react'
import './card-funcionalidad.css'

interface CardFuncionalidadProps {
    funcionalidad: { title: string; text: string };
    imageRight: boolean
}

export default class CardFuncionalidad extends Component<CardFuncionalidadProps> {

  render() {
    const { funcionalidad, imageRight } = this.props;

    return (
        <div className={"card-funcionalidad-container card w-75 my-3 d-flex " + (imageRight ? 'flex-row' : 'flex-row-reverse')}>
            <div className="card-body">
                <h5 className="card-title fw-bold">{funcionalidad.title}</h5>
                <span className="">{funcionalidad.text}</span>
            </div>
            <div className='card-funcionalidad-image'>imagen</div>
        </div>
    )
  }
}
