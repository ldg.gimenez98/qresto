"use client"
import Image from 'next/image'
import React, { Component } from 'react'
import './header.css'

export default class Header extends Component {
  render() {
    return (
      <div className='d-flex flex-row align-items-center justify-content-between w-100 mb-3'>
        <div className='d-flex flex-row align-items-end'>
            <h1 className='qresto-title qresto-roboto-slab fw-bold me-4 mb-0'>QResto</h1>
            <h6 className='qresto-sign qresto-roboto-slab fw-bold'>Carta digital al alcance de un QR</h6>
        </div>

        <Image
          className='mr-0 ml-auto'
          src="/icon_qr.svg"
          alt="QR Logo"
          width={180}
          height={100}
          priority
        />
      </div>
    )
  }
}